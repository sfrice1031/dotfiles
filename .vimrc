"Shawn's vimrc file

"Plug plugin manager 
call plug#begin()
Plug 'ycm-core/YouCompleteMe'
call plug#end()

"UI settings
set number
set relativenumber
syntax on
colorscheme zenburn

